import React, {
  useState,
} from 'react';

const API_CHUCK_JOKE_URL = "http://api.icndb.com/jokes/random";
const API_FRENCH_JOKE_URL = "https://bridge.buddyweb.fr/api/blagues/blagues";

const Joke = () => {
  const [data,setData] = useState("Nothing yet"); //

  const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  const getFrenchJoke = async (e) => {
    e.preventDefault();
    const response = await fetch(API_FRENCH_JOKE_URL);
    const json = await response.json();
    setData(json[getRandomInt(0, 99)].blagues)
  }

  const getChuckJoke = async (e) => {
    e.preventDefault();
    const response = await fetch(API_CHUCK_JOKE_URL);
    const json = await response.json();
    setData(json.value.joke)
  }

  return (
    <div>
      <p>{data}</p>
      <button  onClick={getChuckJoke}>Random Chuck Norris Joke</button>
      <button onClick={getFrenchJoke}>Random French Joke</button>
    </div>
  );
}

export default Joke;
