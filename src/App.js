import React, { Component } from 'react';
import Joke from './Joke';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Joke/>
      </div>
    );
  }
}

export default App;
